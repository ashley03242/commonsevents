﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using CommonsEvents.Business.Services;
using CommonsEvents.Business.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CommonsEvents.Tests
{
    [TestClass]
    public class MemberServiceTests
    {
        private IMemberService _memberService;

        private Mock<IWebClientService> _mockWebClientService;
        private Mock<IConfigSettingsService> _mockConfigSettingsService;

        private const string MnisUrl = "myfakemnisurl";

        [TestInitialize]
        public void Setup()
        {
            _mockWebClientService = new Mock<IWebClientService>();
            _mockConfigSettingsService = new Mock<IConfigSettingsService>();

            _memberService = new MemberService(_mockWebClientService.Object, _mockConfigSettingsService.Object);

            SetupConfigService();
            SetupWebResponse();
        }

        private void SetupConfigService()
        {
            _mockConfigSettingsService.Setup(c => c.MnisBaseUrl).Returns(MnisUrl);
        }

        private void SetupWebResponse()
        {
            var alokDocument = XDocument.Parse(
                @"<Members>
                    <Member Member_Id=""4014"" Dods_Id=""62774"" Pims_Id=""5638"" Clerks_Id=""1102"">
                    <DisplayAs>Alok Sharma</DisplayAs>
                    <ListAs>Sharma, Alok</ListAs>
                    <FullTitle>Rt Hon Alok Sharma MP</FullTitle>
                    <LayingMinisterName>Secretary Alok Sharma</LayingMinisterName>
                    <DateOfBirth>1967-09-07T00:00:00</DateOfBirth>
                    <DateOfDeath xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:nil=""true""/>
                    <Gender>M</Gender>
                    <Party Id=""4"">Conservative</Party>
                    <House>Commons</House>
                    <MemberFrom>Reading West</MemberFrom>
                    <HouseStartDate>2010-05-06T00:00:00</HouseStartDate>
                    <HouseEndDate xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:nil=""true""/>
                    <CurrentStatus Id=""0"" IsActive=""True"">
                    <Name>Current Member</Name>
                    <Reason/>
                    <StartDate>2017-06-08T00:00:00</StartDate>
                    </CurrentStatus>
                    </Member>
                </Members>");

            var borisDocument = XDocument.Parse(
                @"<Members>
                      <Member Member_Id=""1423"" Dods_Id=""25179"" Pims_Id=""1189"" Clerks_Id=""312"">
                      <DisplayAs>Boris Johnson</DisplayAs>
                      <ListAs>Johnson, Boris</ListAs>
                      <FullTitle>Rt Hon Boris Johnson MP</FullTitle>
                      <LayingMinisterName>The Prime Minister</LayingMinisterName>
                      <DateOfBirth>1964-06-19T00:00:00</DateOfBirth>
                      <DateOfDeath xsi:nil=""true"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" />
                      <Gender>M</Gender>
                      <Party Id=""4"">Conservative</Party>
                      <House>Commons</House>
                      <MemberFrom>Uxbridge and South Ruislip</MemberFrom>
                      <HouseStartDate>2015-05-07T00:00:00</HouseStartDate>
                      <HouseEndDate xsi:nil=""true"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" />
                      <CurrentStatus Id=""0"" IsActive=""True"">
                        <Name>Current Member</Name>
                        <Reason />
                        <StartDate>2017-06-08T00:00:00</StartDate>
                      </CurrentStatus>
                    </Member>
                </Members>");

            var michelleDocument = XDocument.Parse(
                @"<Members>
                    <Member Member_Id=""4530"" Dods_Id=""84454"" Pims_Id=""6053"" Clerks_Id=""1297"">
                    <DisplayAs>Michelle Donelan</DisplayAs>
                    <ListAs>Donelan, Michelle</ListAs>
                    <FullTitle>Michelle Donelan MP</FullTitle>
                    <LayingMinisterName/>
                    <DateOfBirth>1984-04-08T00:00:00</DateOfBirth>
                    <DateOfDeath xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:nil=""true""/>
                    <Gender>F</Gender>
                    <Party Id=""4"">Conservative</Party>
                    <House>Commons</House>
                    <MemberFrom>Chippenham</MemberFrom>
                    <HouseStartDate>2015-05-07T00:00:00</HouseStartDate>
                    <HouseEndDate xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:nil=""true""/>
                    <CurrentStatus Id=""0"" IsActive=""True"">
                    <Name>Current Member</Name>
                    <Reason/>
                    <StartDate>2017-06-08T00:00:00</StartDate>
                    </CurrentStatus>
                    </Member>
                 </Members>");

            _mockWebClientService.Setup(w => w.GetXmlWebResponse($"{MnisUrl}/members/query/id=4014/")).Returns(alokDocument);
            _mockWebClientService.Setup(w => w.GetXmlWebResponse($"{MnisUrl}/members/query/id=1423/")).Returns(borisDocument);
            _mockWebClientService.Setup(w => w.GetXmlWebResponse($"{MnisUrl}/members/query/id=4530/")).Returns(michelleDocument);
        }

        [TestMethod]
        public void ReturnsSingleMember()
        {
            var members = _memberService.GetMembers(new List<int> { 4014 }).ToList();

            Assert.IsTrue(members.Count == 1 &&
                          members.Single().MemberId == 4014 &&
                          members.Single().FullTitle == "Rt Hon Alok Sharma MP" &&
                          members.Single().MemberFrom == "Reading West" &&
                          members.Single().Party == "Conservative");
        }

        [TestMethod]
        public void ReturnsMultipleMembers()
        {
            var members = _memberService.GetMembers(new List<int> { 4014, 1423 }).ToList();

            Assert.IsTrue(members.Count == 2 &&
                          members.Count(m => m.MemberId == 4014) == 1 &&
                          members.Count(m => m.MemberId == 1423) == 1 &&
                          members.Single(m => m.MemberId == 4014).FullTitle == "Rt Hon Alok Sharma MP" &&
                          members.Single(m => m.MemberId == 4014).MemberFrom == "Reading West" &&
                          members.Single(m => m.MemberId == 4014).Party == "Conservative" &&
                          members.Single(m => m.MemberId == 1423).FullTitle == "Rt Hon Boris Johnson MP" &&
                          members.Single(m => m.MemberId == 1423).MemberFrom == "Uxbridge and South Ruislip" &&
                          members.Single(m => m.MemberId == 1423).Party == "Conservative");
        }
    }
}