﻿namespace CommonsEvents.UI.Models
{
    public class EventViewModel
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string MemberIds { get; set; }
    }
}