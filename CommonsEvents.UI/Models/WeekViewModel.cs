﻿using System.Collections.Generic;

namespace CommonsEvents.UI.Models
{
    public class WeekViewModel
    {
        public IList<DayViewModel> Days { get; set; }
    }
}