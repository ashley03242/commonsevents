﻿using System.Collections.Generic;

namespace CommonsEvents.UI.Models
{
    public class DayViewModel
    {
        public string Date { get; set; }
        public IList<EventViewModel> Events { get; set; }
    }
}