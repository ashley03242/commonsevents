﻿(function () {
    angular.module("commonsevents").service("eventsService", eventsService);

    eventsService.$inject = ["httpHelper", "urlBuilder", "dateHelper"];

    function eventsService(hopHttp, urlBuilder, dateHelper) {
        return {
            getEventsForWeek: getEventsForWeek
        };

        function getEventsForWeek(date) {
            var url = urlBuilder.buildUrl("Events", "GetWeekEvents");
            var data = { date: dateHelper.getUrlFormatDate(date) };
            return hopHttp.get(url, data);
        }
    }
})();