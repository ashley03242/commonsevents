﻿(function () {
    angular.module("commonsevents").service("membersService", membersService);

    membersService.$inject = ["httpHelper", "urlBuilder"];

    function membersService(httpHelper, urlBuilder) {
        return {
            getMembers: getMembers
        };

        function getMembers(memberIds) {
            var url = urlBuilder.buildUrl("Members", "GetMembers");
            var data = { memberIds: memberIds };
            return httpHelper.get(url, data);
        }
    }
})();