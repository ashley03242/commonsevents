﻿(function () {
    "use strict";
    angular.module("commonsevents").service("urlBuilder", urlBuilder);

    urlBuilder.$inject = ["$location"];

    function urlBuilder($location) {

        function getBaseUrl() {
            var baseUrl = $location.protocol() + "://" + $location.host();

            if ($location.port()) {
                baseUrl = baseUrl + ":" + $location.port();
            }

            return baseUrl;
        }

        return {
            buildUrl: buildUrl,
            getTemplateLocation: getTemplateLocation
        };

        function getTemplateLocation(templatePath) {
            var url = getBaseUrl() + templatePath;
            return url;
        }

        function buildUrl(controller, action, area, parameters) {

            var actionMethod = "";

            if (controller) {
                actionMethod += "/" + controller;
            }

            if (action) {
                actionMethod += "/" + action;
            }

            if (area) {
                actionMethod = "/" + area + actionMethod;
            }

            if (parameters) {
                var params = "?";
                var formattedPairs = [];

                parameters.forEach(function (item) {
                    formattedPairs.push(item.name + "=" + item.value);
                });

                params += formattedPairs.join("&");
                actionMethod += params;
            }

            var url = getBaseUrl() + actionMethod;
            return url;
        }
    }
})();