﻿(function () {
    "use strict";
    angular.module("commonsevents").service("httpHelper", httpHelper);

    httpHelper.$inject = ["$http", "$q"];

    function httpHelper($http, $q) {
        return {
            get: get
        };

        function get(url, params) {
            var deferred = $q.defer();

            $http({
                url: url,
                method: "GET",
                params: params
            }).then(function (result) {
                deferred.resolve(result.data);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }
    }
})();