﻿(function () {
    "use strict";
    angular.module("commonsevents").service("dateHelper", dateHelper);

    dateHelper.$inject = [];

    function dateHelper() {
        return {
            getUrlFormatDate: getUrlFormatDate
        };

        function getUrlFormatDate(stringDate) {

            var dateObject = getJsDateFromDisplayFormat(stringDate);

            if (!dateObject) {
                return dateObject;
            }

            var mm = dateObject.getMonth() + 1;
            var dd = dateObject.getDate();

            return [dateObject.getFullYear(), (mm > 9 ? "" : "0") + mm, (dd > 9 ? "" : "0") + dd].join("-");
        }

        function getJsDateFromDisplayFormat(stringDate) {
            if (!stringDate) {
                return stringDate;
            }

            var dateParts = stringDate.split("/");
            var dateObject = new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
            return dateObject;
        }
    }
})();