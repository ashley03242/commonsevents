﻿(function () {
    "use strict";

    angular.module("commonsevents").directive("day", day);

    day.$inject = ["urlBuilder"];

    function day(urlBuilder) {
        var directive = {
            scope: {
                data: "="
            },
            templateUrl: urlBuilder.getTemplateLocation("/app/directives/day/day.html")
        };

        return directive;
    }
})();