﻿(function () {
    "use strict";

    angular.module("commonsevents").directive("member", member);

    member.$inject = ["urlBuilder"];

    function member(urlBuilder) {
        var directive = {
            scope: {
                data: "="
            },
            templateUrl: urlBuilder.getTemplateLocation("/app/directives/member/member.html")
        };

        return directive;
    }
})();