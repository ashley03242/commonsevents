﻿(function () {
    "use strict";

    angular.module("commonsevents").directive("noItems", noItems);

    noItems.$inject = ["urlBuilder"];

    function noItems(urlBuilder) {
        var directive = {
            scope: {
                text: "@"
            },
            templateUrl: urlBuilder.getTemplateLocation("/app/directives/noitems/noitems.html")
        };

        return directive;
    }
})();