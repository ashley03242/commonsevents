﻿(function () {
    "use strict";

    angular.module("commonsevents").directive("event", event);

    event.$inject = ["urlBuilder"];

    function event(urlBuilder) {
        var directive = {
            scope: {

            },
            bindToController: {
                data: "="
            },
            controller: eventCardController,
            controllerAs: "vm",
            templateUrl: urlBuilder.getTemplateLocation("/app/directives/event/event.html")
        };

        return directive;
    }

    eventCardController.$inject = ["membersService"];

    function eventCardController(membersService) {
        var vm = this;

        vm.showAdditionalInfo = false;
        vm.toggleAdditionalInfo = toggleAdditionalInfo;

        var memberInfoRetrieved = false;

        function toggleAdditionalInfo() {
            if (!memberInfoRetrieved) {
                membersService.getMembers(vm.data.memberIds).then(function (members) {
                    vm.showAdditionalInfo = true;
                    vm.members = members;
                    memberInfoRetrieved = true;
                });
            } else {
                vm.showAdditionalInfo = !vm.showAdditionalInfo;
            }
        }
    }
})();