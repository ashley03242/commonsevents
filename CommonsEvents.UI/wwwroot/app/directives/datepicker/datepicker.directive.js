﻿(function () {
    "use strict";

    angular.module("commonsevents").directive("datePicker", datePicker);

    datePicker.$inject = ["urlBuilder"];

    function datePicker(urlBuilder) {
        return {
            restrict: "E",
            templateUrl: urlBuilder.getTemplateLocation("/app/directives/datepicker/datepicker.html"),
            scope: {
                date: "=",
                onDateChange: "&"
            },
            link: function (scope, element) {
                var defaultOptions = {
                    showOn: "button",
                    buttonImage: "/img/calendar-icon.png",
                    buttonImageOnly: true,
                    dateFormat: "dd/mm/yy",
                    defaultDate: +1,
                    firstDay: 1,
                    onSelect: function (date) {
                        scope.date = date;
                        scope.$apply();

                        if (scope.onDateChange) {
                            scope.onDateChange({});
                        }
                    }
                };

                var $datepicker = $(element).find("input");
                $datepicker.datepicker(defaultOptions);
                $datepicker.attr("placeholder", "Select a date");
            }
        };
    }
})();