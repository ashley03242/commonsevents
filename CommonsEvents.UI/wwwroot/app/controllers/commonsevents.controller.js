﻿(function () {
    "use strict";

    angular.module("commonsevents").controller("eventsController", eventsController);

    eventsController.$inject = ["$scope", "eventsService"];

    function eventsController($scope, eventsService) {
        var vm = this;
        vm.getData = getData;
        vm.date = "";

        function getData() {
            eventsService.getEventsForWeek(vm.date).then(function (data) {
                vm.data = data;
            });
        }
    }
})();