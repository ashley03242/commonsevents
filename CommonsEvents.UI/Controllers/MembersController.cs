﻿using System.Linq;
using CommonsEvents.Business.Services.Interfaces;
using CommonsEvents.Domain.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace CommonsEvents.UI.Controllers
{
    public class MembersController : Controller
    {
        private readonly IMemberService _memberService;

        public MembersController(IMemberService memberService)
        {
            _memberService = memberService;
        }

        [HttpGet]
        public ActionResult GetMembers(string memberIds)
        {
            var memberIdsList = memberIds.ParseDashSeparatedList().ToList();
            var members = _memberService.GetMembers(memberIdsList);

            return Ok(members);
        }
    }
}