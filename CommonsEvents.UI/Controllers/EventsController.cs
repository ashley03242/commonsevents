﻿using System;
using System.Linq;
using CommonsEvents.Business.Services.Interfaces;
using CommonsEvents.UI.Mappers.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CommonsEvents.UI.Controllers
{
    public class EventsController : Controller
    {
        private readonly IEventService _eventService;
        private readonly IWeekMapper _weekMapper;

        public EventsController(IEventService eventService, IWeekMapper weekMapper)
        {
            _eventService = eventService;
            _weekMapper = weekMapper;
        }

        [HttpGet]
        [Route("Events/GetWeekEvents")]
        public IActionResult GetWeekEvents(DateTime? date)
        {
            if (!date.HasValue)
            {
                date = DateTime.Today;
            }

            var events = _eventService.GetEventsForWeek(date.Value).ToList();
            var viewModel = _weekMapper.Map(events);

            return Ok(viewModel);
        }
    }
}