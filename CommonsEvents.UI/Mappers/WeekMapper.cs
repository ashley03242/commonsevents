﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonsEvents.Domain;
using CommonsEvents.UI.Mappers.Interfaces;
using CommonsEvents.UI.Models;

namespace CommonsEvents.UI.Mappers
{
    public class WeekMapper : IWeekMapper
    {
        private readonly IEventMapper _eventMapper;

        public WeekMapper(IEventMapper eventMapper)
        {
            _eventMapper = eventMapper;
        }

        public WeekViewModel Map(IList<Event> events)
        {
            var viewModel = new WeekViewModel
            {
                Days = new List<DayViewModel>()
            };

            var dateRange = GetDateRange(events).OrderBy(d => d);

            foreach (var date in dateRange)
            {
                viewModel.Days.Add(new DayViewModel
                {
                    Date = date.ToLongDateString(),
                    Events = events
                        .Where(e => e.GetDatesForEvent().Contains(date))
                        .Select(_eventMapper.Map)
                        .ToList()
                });
            }

            return viewModel;
        }

        private static IEnumerable<DateTime> GetDateRange(IEnumerable<Event> events)
        {
            return events.SelectMany(e => e.GetDatesForEvent()).Distinct();
        }
    }
}