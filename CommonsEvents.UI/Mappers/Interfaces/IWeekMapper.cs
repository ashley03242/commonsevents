﻿using System.Collections.Generic;
using CommonsEvents.Domain;
using CommonsEvents.UI.Models;

namespace CommonsEvents.UI.Mappers.Interfaces
{
    public interface IWeekMapper : IMapper<IList<Event>, WeekViewModel>
    {
    }
}