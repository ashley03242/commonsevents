﻿using CommonsEvents.Domain;
using CommonsEvents.UI.Models;

namespace CommonsEvents.UI.Mappers.Interfaces
{
    public interface IEventMapper : IMapper<Event, EventViewModel>
    {
    }
}