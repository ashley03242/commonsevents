﻿namespace CommonsEvents.UI.Mappers.Interfaces
{
    public interface IMapper<in TEntity, out TViewModel>
    {
        TViewModel Map(TEntity entity);
    }
}