﻿using CommonsEvents.Domain;
using CommonsEvents.Domain.Extensions;
using CommonsEvents.UI.Mappers.Interfaces;
using CommonsEvents.UI.Models;

namespace CommonsEvents.UI.Mappers
{
    public class EventMapper : IEventMapper
    {
        public EventViewModel Map(Event entity)
        {
            return new EventViewModel
            {
                Category = entity.Category,
                Description = entity.Description,
                EndTime = entity.EndTime.ToTimeString(),
                StartTime = entity.StartTime.ToTimeString(),
                MemberIds = entity.MemberIds.CreateDashSeparatedList()
            };
        }
    }
}