﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace CommonsEvents.Domain
{
    public class Event
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public IEnumerable<EventMember> Members { get; set; }

        public IEnumerable<int> MemberIds => Members?.OrderBy(m => m.SortOrder).Select(m => m.Id);

        public IEnumerable<DateTime> GetDatesForEvent()
        {
            if (StartDate.Date > EndDate.Date)
            {
                throw new DataException("Start date must be on or before end date.");
            }

            var dates = new List<DateTime>();

            for (var date = StartDate; date.Date <= EndDate.Date; date = date.AddDays(1))
            {
                dates.Add(date);
            }

            return dates;
        }
    }
}