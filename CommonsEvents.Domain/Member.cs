﻿namespace CommonsEvents.Domain
{
    public class Member
    {
        public int MemberId { get; set; }
        public string FullTitle { get; set; }
        public string MemberFrom { get; set; }
        public string Party { get; set; }
    }
}