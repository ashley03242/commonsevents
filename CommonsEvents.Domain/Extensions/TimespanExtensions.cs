﻿using System;

namespace CommonsEvents.Domain.Extensions
{
    public static class TimespanExtensions
    {
        public static string ToTimeString(this TimeSpan? timespan)
        {
            return timespan.HasValue ? new DateTime(timespan.Value.Ticks).ToString("HH:mm") : null;
        }
    }
}