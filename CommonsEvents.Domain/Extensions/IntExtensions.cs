﻿using System.Collections.Generic;
using System.Linq;

namespace CommonsEvents.Domain.Extensions
{
    public static class IntExtensions
    {
        public static string CreateDashSeparatedList(this IEnumerable<int> ints)
        {
            return ints == null ? string.Empty : string.Join("-", ints);
        }

        public static IEnumerable<int> ParseDashSeparatedList(this string dashList)
        {
            return dashList == null || !dashList.Any() ? Enumerable.Empty<int>() : dashList.Split('-').Select(int.Parse);
        }
    }
}