﻿using System;

namespace CommonsEvents.Domain.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToUrlString(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd");
        }

        public static DateTime GetWeekStartDate(this DateTime dateTime)
        {
            while (dateTime.DayOfWeek != DayOfWeek.Monday)
            {
                dateTime = dateTime.AddDays(-1);
            }

            return dateTime;
        }

        public static DateTime GetWeekEndDate(this DateTime dateTime)
        {
            while (dateTime.DayOfWeek != DayOfWeek.Sunday)
            {
                dateTime = dateTime.AddDays(1);
            }

            return dateTime;
        }
    }
}