﻿using System;
using CommonsEvents.Business.Services.Interfaces;
using Microsoft.Extensions.Configuration;

namespace CommonsEvents.Business.Services
{
    public class ConfigSettingsService : IConfigSettingsService
    {
        private readonly IConfiguration _configuration;

        public ConfigSettingsService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string MnisBaseUrl => GetSetting<string>("MnisBaseUrl");
        public string CalendarBaseUrl => GetSetting<string>("CalendarBaseUrl");

        private T GetSetting<T>(string key)
        {
            return (T)Convert.ChangeType(_configuration.GetValue<string>(key), typeof(T));
        }
    }
}