﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using CommonsEvents.Business.Helpers;
using CommonsEvents.Business.Services.Interfaces;
using CommonsEvents.Domain;

namespace CommonsEvents.Business.Services
{
    public class MemberService : IMemberService
    {
        private readonly IWebClientService _webClientService;
        private readonly IConfigSettingsService _configSettingsService;

        private const string EventUrl = "/members/query/id={0}/";

        public MemberService(IWebClientService webClientService, IConfigSettingsService configSettingsService)
        {
            _webClientService = webClientService;
            _configSettingsService = configSettingsService;
        }

        public IEnumerable<Member> GetMembers(IList<int> memberIds)
        {
            var elements = memberIds.Select(GetMemberElement);
            return ParseMembers(elements);
        }

        private XElement GetMemberElement(int memberId)
        {
            var url = $"{_configSettingsService.MnisBaseUrl}{string.Format(EventUrl, memberId)}";
            return _webClientService.GetXmlWebResponse(url).Descendants("Member").Single();
        }

        private static IEnumerable<Member> ParseMembers(IEnumerable<XElement> elements)
        {
            return elements.Select(m => new Member
            {
                MemberId = XmlHelper.GetIntAttributeValue(m, "Member_Id"),
                FullTitle = XmlHelper.GetStringElementValue(m, "FullTitle"),
                Party = XmlHelper.GetStringElementValue(m, "Party"),
                MemberFrom = XmlHelper.GetStringElementValue(m, "MemberFrom")
            });
        }
    }
}