﻿using System;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Xml.Linq;
using CommonsEvents.Business.Services.Interfaces;
using Newtonsoft.Json;

namespace CommonsEvents.Business.Services
{
    public class WebClientService : IWebClientService
    {
        public TEntity GetJsonWebResponse<TEntity>(string url)
        {
            return GetWebResponse(JsonConvert.DeserializeObject<TEntity>, url);
        }

        public XDocument GetXmlWebResponse(string url)
        {
            return GetWebResponse(XDocument.Parse, url);
        }

        private static T GetWebResponse<T>(Func<string, T> func, string url)
        {
            var tries = 0;

            while (tries < 3)
            {
                try
                {
                    tries++;

                    var request = WebRequest.Create(url);
                    request.CachePolicy = new HttpRequestCachePolicy(HttpRequestCacheLevel.NoCacheNoStore);
                    using (var response = request.GetResponse())
                    {
                        using (var responseStream = response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(responseStream))
                            {
                                var endpointResponse = reader.ReadToEnd();
                                var data = func.Invoke(endpointResponse);
                                return data;
                            }
                        }
                    }
                }
                catch
                {
                    if (tries >= 3)
                    {
                        throw;
                    }
                }
            }

            throw new ApplicationException("The Web client service failed to request data from the following Url: " + url);
        }
    }
}