﻿namespace CommonsEvents.Business.Services.Interfaces
{
    public interface IConfigSettingsService
    {
        string MnisBaseUrl { get; }
        string CalendarBaseUrl { get; }
    }
}