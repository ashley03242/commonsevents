﻿using System;
using System.Collections.Generic;
using CommonsEvents.Domain;

namespace CommonsEvents.Business.Services.Interfaces
{
    public interface IEventService
    {
        IEnumerable<Event> GetEventsForWeek(DateTime date);
    }
}