﻿using System.Collections.Generic;
using CommonsEvents.Domain;

namespace CommonsEvents.Business.Services.Interfaces
{
    public interface IMemberService
    {
        IEnumerable<Member> GetMembers(IList<int> memberIds);
    }
}