﻿using System.Xml.Linq;

namespace CommonsEvents.Business.Services.Interfaces
{
    public interface IWebClientService
    {
        TEntity GetJsonWebResponse<TEntity>(string url);
        XDocument GetXmlWebResponse(string url);
    }
}