﻿using System;
using System.Collections.Generic;
using CommonsEvents.Business.Services.Interfaces;
using CommonsEvents.Domain;
using CommonsEvents.Domain.Extensions;

namespace CommonsEvents.Business.Services
{
    public class EventService : IEventService
    {
        private readonly IWebClientService _webClientService;
        private readonly IConfigSettingsService _configSettingsService;

        private const string EventUrl = "/calendar/events/list.json?startdate={0}&enddate={1}&house=commons&eventtypeid=32";

        public EventService(IWebClientService webClientService, IConfigSettingsService configSettingsService)
        {
            _webClientService = webClientService;
            _configSettingsService = configSettingsService;
        }

        public IEnumerable<Event> GetEventsForWeek(DateTime date)
        {
            var startOfWeek = date.GetWeekStartDate().ToUrlString();
            var endOfWeek = date.GetWeekEndDate().ToUrlString();

            var url = $"{_configSettingsService.CalendarBaseUrl}{string.Format(EventUrl, startOfWeek, endOfWeek)}";
            var events = _webClientService.GetJsonWebResponse<List<Event>>(url);

            return events;
        }
    }
}