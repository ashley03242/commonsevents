﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace CommonsEvents.Business.Helpers
{
    public static class XmlHelper
    {
        public static int GetIntAttributeValue(XElement element, string valueAttributeName)
        {
            return Convert.ToInt32(GetStringAttributeValue(element, valueAttributeName));
        }

        public static int GetIntElementValue(XElement element, string valueAttributeName)
        {
            return Convert.ToInt32(GetStringElementValue(element, valueAttributeName));
        }

        public static DateTime GetDateTimeElementValue(XElement element, string valueElementName)
        {
            return Convert.ToDateTime(GetStringElementValue(element, valueElementName));
        }

        public static TimeSpan GetTimeSpanElementValue(XElement element, string valueElementName)
        {
            return TimeSpan.Parse(GetStringElementValue(element, valueElementName));
        }

        public static TimeSpan? GetNullableTimespanElementValue(XElement element, string valueElementName)
        {
            if (TimeSpan.TryParse(GetStringElementValue(element, valueElementName), out var timeSpan))
            {
                return timeSpan;
            }

            return null;
        }

        public static DateTime? GetNullableDateTimeElementValue(XElement element, string valueElementName)
        {
            var stringValue = GetStringElementValue(element, valueElementName);

            if (DateTime.TryParse(stringValue, out var dateTime))
            {
                return dateTime;
            }

            return null;
        }

        public static bool GetBoolElementValue(XElement element, string valueElementName)
        {
            return Convert.ToBoolean(GetStringElementValue(element, valueElementName));
        }

        public static string GetStringAttributeValue(XElement element, string valueAttributeName)
        {
            var attribute = element.Attributes(valueAttributeName).SingleOrDefault();
            return attribute?.Value;
        }

        public static string GetStringElementValue(XElement element, string valueElementName)
        {
            var value = element.Elements(valueElementName).SingleOrDefault();
            return value?.Value;
        }
    }
}